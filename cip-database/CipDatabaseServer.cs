﻿using System;
using System.Data;
using System.Collections.Generic;

using CitizenFX.Core;
using MySql.Data.MySqlClient;

using static CitizenFX.Core.Native.API;

namespace cip_database
{
    public class CipDatabaseServer : BaseScript
    {
        private MySqlConnection connection;

        public CipDatabaseServer()
        {
            string convarServer = GetDatabaseConvar<string>("db_server");
            uint convarPort = GetDatabaseConvar<uint>("db_port");
            string convarUser = GetDatabaseConvar<string>("db_user");
            string convarPassword = GetDatabaseConvar<string>("db_password");
            string convarDatabase = GetDatabaseConvar<string>("db_database");
            uint convarWorkers = GetDatabaseConvar<uint>("db_workers");

            MySqlConnectionStringBuilder connectionString = new MySqlConnectionStringBuilder()
            {
                Server = convarServer,
                Port = convarPort,
                UserID = convarUser,
                Password = convarPassword,
                Database = convarDatabase,
                AllowUserVariables = true
            };

            connection = new MySqlConnection(connectionString.ToString());

            if (connection != null)
            {
                try
                {
                    connection.Open();
                    Debug.WriteLine($"^2[CIP-DATABASE] Connected to {connectionString.Server}:{connectionString.Port} with user {connectionString.UserID}^7");
                }
                catch (MySqlException exception)
                {
                    Debug.WriteLine($"^1[CIP-DATABASE] Exception was thrown when trying to open a connection with message: `{exception.Message}`^7");
                    return;
                }
            }
            else
            {
                Debug.WriteLine($"^1[CIP-DATABASE] The connection instance is not present.^7");
                return;
            }

            Exports.Add("ChangeDatabase", new Action<string>((dbName) =>
            {
                if (!IsConnectionAvailable()) return;

                if(string.IsNullOrEmpty(dbName))
                {
                    Debug.WriteLine($"^1[CIP-DATABASE] Cannot change database to an empty string..^7");
                    return;
                }

                try
                {
                    connection.ChangeDatabase(dbName);
                    Debug.WriteLine($"^2[CIP-DATABASE] The current connection now uses the `{dbName}` database");
                }
                catch(Exception exception)
                {
                    Debug.WriteLine($"^1[CIP-DATABASE] Exception was thrown when trying to change database with message: `{exception.Message}`^7");
                }

            }));

            Exports.Add("ExecuteNonQuery", new Action<string, IDictionary<string, object>>((query, parameters) =>
            {
                if (!IsConnectionAvailable()) return;

                try
                {
                    MySqlCommand command = PrepareCommand(query, parameters);

                    foreach (var param in parameters)
                    {
                        command.Parameters.AddWithValue(param.Key, param.Value);
                    }

                    command.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Debug.WriteLine($"^1[CIP-DATABASE] Exception was thrown when trying to execute a query (ExecuteNonQuery) with message: `{exception.Message}`^7");
                }
            }));

            Exports.Add("ExecuteNonQueryResult", new Func<string, IDictionary<string, object>, int>((query, parameters) =>
            {
                if (!IsConnectionAvailable()) return -1;

                try
                {
                    MySqlCommand command = PrepareCommand(query, parameters);
                    return command.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Debug.WriteLine($"^1[CIP-DATABASE] Exception was thrown when trying to execute a query (ExecuteNonQuery) with message: `{exception.Message}`^7");
                }

                return -1;
            }));
        }

        private bool IsConnectionAvailable()
        {
            bool isConnectionAvailable = connection != null && connection.State == ConnectionState.Open;  
            
            if(!isConnectionAvailable)
            {
                Debug.WriteLine($"^1[CIP-DATABASE] Trying to run a command on closed or a non-existent connection...^7");
                return false;
            }

            return true;
        }

        private MySqlCommand PrepareCommand(string query, IDictionary<string, object> parameters)
        {
            MySqlCommand command = connection.CreateCommand();
            command.CommandText = query;

            foreach (var param in parameters)
            {
                command.Parameters.AddWithValue(param.Key, param.Value);
            }

            return command;
        }

        private T GetDatabaseConvar<T>(string convarName)
        {
            string value = GetConvar(convarName, "");

            if (!string.IsNullOrEmpty(value))
            {
                return (T)Convert.ChangeType(value, typeof(T));
            }
            else
            {
                Debug.WriteLine($"[CIP-DATABASE] Missing convar {convarName}, have you used `set {convarName} 'value'?`");
                throw new ArgumentException("Failed to initialize due to missing convars.");
            }
        }
    }
}
